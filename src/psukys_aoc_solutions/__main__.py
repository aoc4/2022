"""CLI frontend logic for package"""

import argparse
import importlib
import logging
from pathlib import Path
import sys
from typing import Iterable, Tuple, Type

from aocd import get_data

from .day import Day

_REPO_DIR = Path(__file__).absolute().parent.parent.parent
_SRC_DIR = _REPO_DIR / "src" / "psukys_aoc_solutions"
_TEST_DIR = _REPO_DIR / "tests"

DAYS_SOLVED = 15


def _argparser():
    """Setup CLI argument parser"""
    parser = argparse.ArgumentParser("Advent of Code solutions")
    parser.add_argument("--make-day", type=int, required=False)
    parser.add_argument("--solve", action="store_true", required=False)
    parser.add_argument("day", type=int, default=1)
    return parser


def _make_day(day: int):
    if (_SRC_DIR / f"day{day}.py").exists():
        logging.warn("Day already exists")
        return
    logging.info(f"Creating day {day}")
    with open(_SRC_DIR / f"day{day}.py", "w") as solution_file:
        solution_file.write(
            f"""class Day{day}:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        ...

    def solution2(self):
        ...
            """
        )

    with open(_TEST_DIR / f"test_day{day}.py", "w") as test_file:
        test_file.write(
            f"""import pytest

from psukys_aoc_solutions.day{day} import Day{day}


def test_1():
    data = ""
    expected = None
    sut = Day{day}(data=data)
    assert expected == sut.solution1()

@pytest.mark.skip("depends on first task solved")
def test_2():
    data = ""
    expected = None
    sut = Day{day}(data=data)
    assert expected == sut.solution2()
"""
        )


def _iter_days_with_data(start: int) -> Iterable[Tuple[Type[Day], int]]:
    for day in range(start, DAYS_SOLVED + 1):
        day_cls = getattr(
            importlib.import_module(f"psukys_aoc_solutions.day{day}"), f"Day{day}"
        )
        yield day_cls, get_data(year=2022, day=day)


def _solve(day: int):
    for day_cls, data in _iter_days_with_data(day):
        day: Day = day_cls(data)
        logging.info(day.__class__.__name__)
        logging.info(f"solution 1: {day.solution1()}")
        day: Day = day_cls(data)  # for cases when solution1 mutates internal data
        logging.info(f"solution 2: {day.solution2()}")


def main():
    """Starting function for CLI interaction"""
    logging.basicConfig(
        level=logging.INFO,
        format="%(asctime)s %(message)s",
        handlers=[logging.StreamHandler()],
    )
    parser = _argparser()
    args = parser.parse_args()
    if args.make_day:
        _make_day(args.make_day)
        sys.exit(0)

    if args.solve:
        _solve(args.day)
        sys.exit(0)


if __name__ == "__main__":
    main()
