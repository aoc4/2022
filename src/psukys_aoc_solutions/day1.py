class Day1:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        return max(get_calorie_sums(self.data))

    def solution2(self):
        sums = sorted(get_calorie_sums(self.data), reverse=True)
        return sum(sums[:3])


def get_calorie_sums(data: str) -> list[int]:
    elf_food_calories = [
        [int(calories) for calories in elf_calories.splitlines()]
        for elf_calories in data.split("\n\n")
    ]
    return [sum(elf_calories) for elf_calories in elf_food_calories]
