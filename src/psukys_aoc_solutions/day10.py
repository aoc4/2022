class Day10:
    def __init__(self, data: str):
        self.instructions = data.splitlines()

    def solution1(self):
        return sum(self.cycle(num) * num for num in [20, 60, 100, 140, 180, 220])

    # smells very cacheable, but let's see...
    def cycle(self, num: int) -> int:
        """Returns register value DURING cycle"""
        register = 1
        cycle_count = 0
        for instruction in self.instructions:
            if instruction == "noop":
                cycle_count += 1
            else:
                addition = int(instruction.split()[1])
                cycle_count += 1
                if cycle_count == num:
                    return register
                cycle_count += 1
                if cycle_count == num:
                    return register
                register += addition
            if cycle_count == num:
                return register
        raise ValueError(f"Cycle {num} is not reached. Max: {cycle_count}")

    def solution2(self):
        crt = ["." for _ in range(241)]
        current_cycle = 0
        register = 1

        def draw():
            nonlocal current_cycle
            crt[current_cycle] = (
                "#" if register - 1 <= current_cycle % 40 <= register + 1 else "."
            )
            current_cycle += 1

        for instruction in self.instructions:
            if instruction == "noop":
                draw()
            else:
                addition = int(instruction.split()[1])
                draw()
                draw()
                register += addition

        return "\n".join(["".join(crt[row * 40 : (row + 1) * 40]) for row in range(6)])
