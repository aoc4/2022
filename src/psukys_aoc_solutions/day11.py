from dataclasses import dataclass
from typing import Callable
from functools import cache
from math import prod


@dataclass
class Monkey:
    items: list[int]
    operation: Callable[[int], int]
    division_test: int
    success_target: int
    fail_target: int
    inspections: int = 0


def parse_monkey(data: str) -> Monkey:
    items, op, test, target_true, target_false = data.splitlines()[1:]
    start_items = [int(item.strip()) for item in items.split(":")[1].split(",")]
    operation = parse_operation(op.split(":")[1])
    division = int(test.split("by")[1].strip())
    success_target = int(target_true.split("monkey")[1].strip())
    fail_target = int(target_false.split("monkey")[1].strip())
    return Monkey(
        items=start_items,
        operation=operation,
        division_test=division,
        success_target=success_target,
        fail_target=fail_target,
    )


def parse_operation(operation: str) -> Callable[[int], int]:
    # shortcut by inspecting data:
    right_side = operation.split("=")[1]
    # foresight on right side formation
    _, symbol, right = right_side.split()
    if symbol == "+":
        if right.isnumeric():
            value = int(right)
            return lambda old: old + value
        else:
            return lambda old: old + old
    if symbol == "*":
        if right.isnumeric():
            value = int(right)
            return lambda old: old * value
        else:
            return lambda old: old * old
    raise ValueError(f"Didn't expect {symbol} operand")


class Day11:
    def __init__(self, data: str):
        self.monkeys = [parse_monkey(monkey_data) for monkey_data in data.split("\n\n")]

    def solution1(self):
        for _ in range(20):
            for monkey in self.monkeys:
                inspected_items = [monkey.operation(item) // 3 for item in monkey.items]
                monkey.items = []
                for item in inspected_items:
                    monkey.inspections += 1
                    if item % monkey.division_test == 0:
                        self.monkeys[monkey.success_target].items.append(item)
                    else:
                        self.monkeys[monkey.fail_target].items.append(item)
        inspections = sorted(monkey.inspections for monkey in self.monkeys)[-2:]
        return prod(inspections)

    def solution2(self):
        common_divider = prod(monkey.division_test for monkey in self.monkeys)
        for _ in range(10_000):
            for monkey in self.monkeys:
                inspected_items = [
                    monkey.operation(item) % common_divider for item in monkey.items
                ]
                monkey.items = []
                for item in inspected_items:
                    monkey.inspections += 1
                    if item % monkey.division_test == 0:
                        self.monkeys[monkey.success_target].items.append(item)
                    else:
                        self.monkeys[monkey.fail_target].items.append(item)
        inspections = sorted(monkey.inspections for monkey in self.monkeys)[-2:]
        return prod(inspections)
