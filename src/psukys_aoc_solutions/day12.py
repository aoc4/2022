from dataclasses import dataclass, field
import networkx as nx


class Day12:
    def __init__(self, data: str):
        self.area = [list(line) for line in data.splitlines()]

    def solution1(self):
        return get_shortest_path_length(self.area) - 1

    def solution2(self):
        return min(get_shortest_path_lengths(self.area)) - 1


@dataclass(frozen=True, unsafe_hash=True)
class Point:
    x: int = field(hash=True)
    y: int = field(hash=True)


def get_neighbours(point: Point, max_point: Point) -> set[Point]:
    neighbours = []

    # top
    if point.y > 0:
        neighbours.append(Point(point.x, point.y - 1))

    # right
    if point.x < max_point.x:
        neighbours.append(Point(point.x + 1, point.y))

    # bottom
    if point.y < max_point.y:
        neighbours.append(Point(point.x, point.y + 1))

    # left
    if point.x > 0:
        neighbours.append(Point(point.x - 1, point.y))

    return set(neighbours)


def filter_climbable_heights(
    area: list[list[str]], current_point: Point, neighbours: list[Point]
) -> list[Point]:
    adapter = lambda x: "a" if x == "S" else ("z" if x == "E" else x)
    current_height = ord(adapter(area[current_point.y][current_point.x]))
    return [
        neighbour
        for neighbour in neighbours
        if (ord(adapter(area[neighbour.y][neighbour.x])) - current_height) <= 1
    ]


def get_shortest_path_length(area: list[list[str]]) -> int:
    graph = nx.DiGraph()
    start_point = [
        Point(x, y)
        for x in range(len(area[0]))
        for y in range(len(area))
        if area[y][x] == "S"
    ][0]
    end_point = [
        Point(x, y)
        for x in range(len(area[0]))
        for y in range(len(area))
        if area[y][x] == "E"
    ][0]
    max_point = Point(len(area[0]) - 1, len(area) - 1)
    for y in range(len(area)):
        for x in range(len(area[0])):
            curr_point = Point(x, y)
            neighbours = get_neighbours(curr_point, max_point)
            filtered = filter_climbable_heights(area, curr_point, neighbours)
            for neighbour in filtered:
                graph.add_edge(curr_point, neighbour)

    return len(nx.shortest_path(graph, start_point, end_point))


def get_shortest_path_lengths(area: list[list[str]]) -> list[int]:
    graph = nx.DiGraph()
    end_point = [
        Point(x, y)
        for x in range(len(area[0]))
        for y in range(len(area))
        if area[y][x] == "E"
    ][0]
    max_point = Point(len(area[0]) - 1, len(area) - 1)
    for y in range(len(area)):
        for x in range(len(area[0])):
            curr_point = Point(x, y)
            neighbours = get_neighbours(curr_point, max_point)
            filtered = filter_climbable_heights(area, curr_point, neighbours)
            for neighbour in filtered:
                graph.add_edge(curr_point, neighbour)

    start_points = [
        Point(x, y)
        for x in range(len(area[0]))
        for y in range(len(area))
        if area[y][x] in ["S", "a"]
    ]

    paths = []
    for start_point in start_points:
        if nx.has_path(graph, start_point, end_point):
            paths.append(len(nx.shortest_path(graph, start_point, end_point)))
    return paths
