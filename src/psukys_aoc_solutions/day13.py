import json
from functools import cmp_to_key
from math import prod


class Day13:
    def __init__(self, data: str):
        self.data = [
            [json.loads(line) for line in pair.splitlines()]
            for pair in data.split("\n\n")
        ]

    def solution1(self):
        return sum(
            index + 1
            for index, pair in enumerate(self.data)
            if compare(pair[0], pair[1]) > 0
        )

    def solution2(self):
        data = sorted(
            [line for pair in self.data for line in pair] + [[[2]], [[6]]],
            key=cmp_to_key(compare),
            reverse=True,
        )
        return prod(i + 1 for i, item in enumerate(data) if item in ([[2]], [[6]]))


def compare(left, right) -> bool:
    left = left if isinstance(left, list) else [left]
    right = right if isinstance(right, list) else [right]
    for l, r in zip(left, right):
        if isinstance(l, list) or isinstance(r, list):
            res = compare(l, r)
        else:
            res = r - l
        if res != 0:
            return res
    return len(right) - len(left)
