from dataclasses import dataclass


@dataclass(frozen=True)
class Point:
    x: int
    y: int


class AbyssException(Exception):
    pass


class Day14:
    SAND_SOURCE = Point(500, 0)

    def __init__(self, data: str):
        walls = parse_obstacles(data)
        self.obstacles = set()
        for wall in walls:
            start = wall[0]
            for next in wall[1:]:
                self.obstacles.update(get_points(start, next))
                start = next
        self.initial_obstacle_count = len(self.obstacles)
        self.abyss_depth = max(point.y for point in self.obstacles)

    def solution1(self):
        while True:
            try:
                # due to recursion, it cycles after last one lands (or goes into abyss)
                self.drop(self.SAND_SOURCE)
            except AbyssException:
                return len(self.obstacles) - self.initial_obstacle_count

    def drop(self, location: Point):
        """Attempt to drop sand from specific point

        Arguments:
            location: starting point where to look to drop

        Raises:
            AbyssException: when drop goes beyond map (abyss)

        """
        if location.y == self.abyss_depth:
            raise AbyssException("Ooooo")

        if location in self.obstacles:
            raise AbyssException("Beep boop overload")

        checks = [
            Point(location.x, location.y + 1),  # step down
            Point(location.x - 1, location.y + 1),  # step down left
            Point(location.x + 1, location.y + 1),  # step down right
        ]
        for check in checks:
            if check not in self.obstacles:
                return self.drop(check)
        self.obstacles.add(location)

    def solution2(self):
        # dunno lol, just guessing
        self.abyss_depth += 2
        min_x = min(point.x for point in self.obstacles) - self.abyss_depth
        max_x = max(point.x for point in self.obstacles) + self.abyss_depth

        for x in range(min_x, max_x):
            self.obstacles.add(Point(x, self.abyss_depth))

        self.initial_obstacle_count = len(self.obstacles)

        while True:
            try:
                # due to recursion, it cycles after last one lands (or goes into abyss)
                self.drop(self.SAND_SOURCE)
            except AbyssException:
                return len(self.obstacles) - self.initial_obstacle_count


def parse_obstacles(data: str) -> list[list[Point]]:
    return [
        [
            Point(*[int(item) for item in entry.split(",")])
            for entry in line.split(" -> ")
        ]
        for line in data.splitlines()
    ]


def get_points(start: Point, end: Point) -> set[Point]:
    if start.x == end.x:
        if start.y > end.y:
            ys = range(end.y, start.y + 1)
        else:
            ys = range(start.y, end.y + 1)
        return set(Point(start.x, y) for y in ys)
    elif start.y == end.y:
        if start.x > end.x:
            xs = range(end.x, start.x + 1)
        else:
            xs = range(start.x, end.x + 1)
        return set(Point(x, start.y) for x in xs)
    else:
        raise RuntimeError("Only vertical or horizontal wall supported")
