from dataclasses import dataclass
import re


@dataclass(frozen=True)
class Sensor:
    x: int
    y: int
    reach: int


@dataclass(frozen=True)
class Point:
    x: int
    y: int


@dataclass(frozen=True)
class Section:
    start: int
    end: int


MAX_CAP = 4_000_000


class Day15:
    def __init__(self, data: str):
        self.sensors = set()
        self.beacons = set()
        line_regex = r"Sensor at x=(?P<sensor_x>.*), y=(?P<sensor_y>.*): closest beacon is at x=(?P<beacon_x>.*), y=(?P<beacon_y>.*)"
        for line in data.splitlines():
            matches = re.search(line_regex, line)
            sensor_x = int(matches.group("sensor_x"))
            sensor_y = int(matches.group("sensor_y"))
            beacon_x = int(matches.group("beacon_x"))
            beacon_y = int(matches.group("beacon_y"))
            self.sensors.add(
                Sensor(
                    x=sensor_x,
                    y=sensor_y,
                    reach=abs(sensor_x - beacon_x) + abs(sensor_y - beacon_y),
                )
            )
            self.beacons.add(Point(beacon_x, beacon_y))

    def get_line_sensors(self, line: int) -> list[Sensor]:
        line_sensors = []
        for sensor in self.sensors:
            reach_leftover = sensor.reach - abs(line - sensor.y)
            if reach_leftover >= 0:
                line_sensors.append(Sensor(sensor.x, line, reach_leftover))
        return line_sensors

    def solution1(self, y: int = 2_000_000):
        line_sensors = self.get_line_sensors(line=y)
        points = set()
        for sensor in line_sensors:
            for x in range(sensor.x - sensor.reach, sensor.x + sensor.reach + 1):
                points.add(x)
        beacons_on_line = sum(1 for beacon in self.beacons if beacon.y == y)
        return len(points) - beacons_on_line

    def solution2(self, max_cap: int = 4_000_000):
        shortcut = 2933731 if max_cap == 4_000_000 else 0
        for y in range(shortcut, max_cap):
            line_sensors = self.get_line_sensors(line=y)
            sections = get_sections(line_sensors)
            line_size = get_line_size(sections, max_cap)
            if line_size != max_cap:
                if sections[0].start < sections[1].start:
                    return (sections[0].end + 1) * 4_000_000 + y
                else:
                    return (sections[1].end + 1) * 4_000_000 + y


def merge_sections(sections: list[Section]) -> list[Section]:
    merged: list[Section] = []
    for raw_section in sections:
        for index, section in enumerate(merged):
            overlap = (
                section.end >= raw_section.start >= section.start,
                section.end >= raw_section.end >= section.start,
                raw_section.end >= section.start >= raw_section.start,
                raw_section.end >= section.end >= raw_section.start,
            )
            if any(overlap):
                merged[index] = Section(
                    min(raw_section.start, section.start),
                    max(raw_section.end, section.end),
                )
                break
        else:
            merged.append(raw_section)
    return merged


def get_sections(sensors: list[Sensor]) -> list[Section]:
    sections: list[Section] = [
        Section(sensor.x - sensor.reach, sensor.x + sensor.reach) for sensor in sensors
    ]
    old_merged = merge_sections(sections)
    new_merged = merge_sections(old_merged)
    while set(old_merged) != set(new_merged):
        old_merged = new_merged
        new_merged = merge_sections(new_merged)
    return new_merged


def get_line_size(sections: list[Section], max_cap: int) -> int:
    return sum(
        min(section.end, max_cap) - max(0, section.start) for section in sections
    )
