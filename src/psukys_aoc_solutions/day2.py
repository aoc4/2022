from enum import Enum


class Items(Enum):
    ROCK = 1
    PAPER = 2
    SCISSORS = 3


class Results(Enum):
    WIN = 6
    DRAW = 3
    LOSE = 0


OPPONENT = {"A": Items.ROCK, "B": Items.PAPER, "C": Items.SCISSORS}


class Day2:
    def __init__(self, data: str):
        self.data = [line.split() for line in data.splitlines()]

    def solution1(self):
        ME = {"X": Items.ROCK, "Y": Items.PAPER, "Z": Items.SCISSORS}
        return sum(
            solution1_score(OPPONENT[opponent], ME[me]) for opponent, me in self.data
        )

    def solution2(self):
        RESULT = {"X": Results.LOSE, "Y": Results.DRAW, "Z": Results.WIN}
        return sum(
            solution2_score(OPPONENT[opponent], RESULT[result])
            for opponent, result in self.data
        )


def solution1_score(opponent: Items, me: Items) -> int:
    if opponent == Items.ROCK:
        if me == Items.ROCK:
            return me.value + Results.DRAW.value
        if me == Items.PAPER:
            return me.value + Results.WIN.value
        return me.value + Results.LOSE.value  # SCISSORS
    if opponent == Items.PAPER:
        if me == Items.ROCK:
            return me.value + Results.LOSE.value
        if me == Items.PAPER:
            return me.value + Results.DRAW.value
        return me.value + Results.WIN.value  # SCISSORS
    # SCISSORS
    if me == Items.ROCK:
        return me.value + Results.WIN.value
    if me == Items.PAPER:
        return me.value + Results.LOSE.value
    return me.value + Results.DRAW.value  # SCISSORS


def solution2_score(opponent: Items, result: Results) -> int:
    if result == Results.WIN:
        if opponent == Items.ROCK:
            return result.value + Items.PAPER.value
        if opponent == Items.PAPER:
            return result.value + Items.SCISSORS.value
        # SCISSORS
        return result.value + Items.ROCK.value
    if result == Results.DRAW:
        # values are the same for all cases when in draw
        return result.value + opponent.value
    # LOSE
    if opponent == Items.ROCK:
        return result.value + Items.SCISSORS.value
    if opponent == Items.PAPER:
        return result.value + Items.ROCK.value
    # SCISSORS
    return result.value + Items.PAPER.value
