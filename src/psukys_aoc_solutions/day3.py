class Day3:
    def __init__(self, data: str):
        self.data = data.splitlines()

    def solution1(self):
        priority_sum = 0
        for rucksack in self.data:
            comp1, comp2 = split_compartments(rucksack)
            common = comp1.intersection(comp2)
            priority_sum += sum(get_priority(char) for char in common)
        return priority_sum

    def solution2(self):
        priority_sum = 0
        for rucksacks in chunkify(self.data, 3):
            common_badges = set()
            for rucksack in rucksacks:
                items = set(rucksack)
                common_badges = common_badges.intersection(items) or items
            priority_sum += get_priority(common_badges.pop())
        return priority_sum


def chunkify(items, step):
    for i in range(len(items) // step):
        yield items[i * step : (i + 1) * step]


def split_compartments(rucksack: str) -> tuple[set[str], set[str]]:
    comp1 = set(list(rucksack[: len(rucksack) // 2]))
    comp2 = set(list(rucksack[len(rucksack) // 2 :]))
    return comp1, comp2


def get_priority(char: str) -> int:
    if char.isupper():
        return ord(char) - ord("A") + 27
    return ord(char) - ord("a") + 1
