from dataclasses import dataclass


@dataclass
class Assignment:
    start: int
    end: int

    def __contains__(self, other: "Assignment") -> bool:
        return self.start >= other.start and self.end <= other.end

    def overlaps(self, other: "Assignment") -> bool:
        return self.start <= other.end and self.end >= other.start


class Day4:
    def __init__(self, data: str):
        self.data = []
        for line in data.splitlines():
            first, second = line.split(",")
            self.data.append(
                (
                    Assignment(*[int(item) for item in first.split("-")]),
                    Assignment(*[int(item) for item in second.split("-")]),
                )
            )

    def solution1(self):
        count = 0
        for first, second in self.data:
            count += first in second or second in first
        return count

    def solution2(self):
        count = 0
        for first, second in self.data:
            count += first.overlaps(second) or second.overlaps(first)
        return count
