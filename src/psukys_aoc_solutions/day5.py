from dataclasses import dataclass


@dataclass
class Instruction:
    source: int
    target: int
    amount: int


class Day5:
    def __init__(self, data: str):
        self.data = data

    def get_crates_and_instructions(self) -> tuple[list[list[str]], list[Instruction]]:
        # solutions mutate crates
        crates, instructions = self.data.split("\n\n")
        return parse_crates(crates), parse_instructions(instructions)

    def solution1(self):
        crates, instructions = self.get_crates_and_instructions()
        for instruction in instructions:
            for _ in range(instruction.amount):
                crates[instruction.target - 1].append(
                    crates[instruction.source - 1].pop()
                )
        top_crates = [crate[-1] for crate in crates]
        return "".join(top_crates)

    def solution2(self):
        crates, instructions = self.get_crates_and_instructions()
        for instruction in instructions:
            pop_start = len(crates[instruction.source - 1]) - instruction.amount
            for _ in range(instruction.amount):
                crates[instruction.target - 1].append(
                    crates[instruction.source - 1].pop(pop_start)
                )
        top_crates = [crate[-1] for crate in crates]
        return "".join(top_crates)


def parse_crates(crates_data: str) -> list[list[str]]:
    crate_lines = crates_data.splitlines()[:-1]
    crates = [[] for _ in range(1, len(crate_lines[0]), 4)]
    for line in crate_lines:
        for index, crate_index in enumerate(range(1, len(line), 4)):
            # reading top to bottom, so before last element
            crate = line[crate_index]
            if crate != " ":
                crates[index].insert(0, crate)
    return crates


def parse_instructions(instructions_data: str) -> list[Instruction]:
    instructions = []
    for line in instructions_data.splitlines():
        words = line.split()
        instructions.append(
            Instruction(
                source=int(words[3]), target=int(words[5]), amount=int(words[1])
            )
        )
    return instructions
