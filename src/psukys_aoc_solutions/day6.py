class Day6:
    def __init__(self, data: str):
        self.data = data

    def solution1(self):
        return find_unique(self.data, 4)

    def solution2(self):
        return find_unique(self.data, 14)


def find_unique(data: str, window: int) -> int:
    for i in range(len(data) - window + 1):
        if all_unique(data[i : i + window], window):
            return i + window


def all_unique(letters: str, window: int) -> bool:
    return len(set(letters)) == window
