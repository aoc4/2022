from dataclasses import dataclass
from typing import Optional, Union


@dataclass
class File:
    name: str
    size: int


@dataclass
class Directory:
    name: str
    parent: Optional["Directory"]
    children: list[Union[File, "Directory"]]

    @property  # a trick to match File's attrs
    def size(self) -> int:
        return sum(child.size for child in self.children)


class Day7:
    def __init__(self, data: str):
        self.directory = parse_command_output(data)

    def solution1(self):
        # walk through directories and count that have <100k size
        SIZE_THRESHOLD = 100_000
        return count_threshold_directory_sizes(
            items=[self.directory], threshold=SIZE_THRESHOLD
        )

    def solution2(self):
        MAX_CAPACITY = 70_000_000
        UNUSED_SPACE_REQUIRED = 30_000_000
        used_space = self.directory.size
        sizes = sorted(get_directory_sizes([self.directory]))
        missing_capacity = used_space - (MAX_CAPACITY - UNUSED_SPACE_REQUIRED)

        if missing_capacity <= 0:
            return 0

        for size in sizes:
            if size >= missing_capacity:
                return size


def get_directory_sizes(directories: list[File | Directory]) -> list[int]:
    sizes = []
    for item in directories:
        if isinstance(item, Directory):
            sizes.append(item.size)
            sizes.extend(get_directory_sizes(item.children))
    return sizes


def count_threshold_directory_sizes(
    items: list[File | Directory], threshold: int
) -> int:
    count = 0
    for child in items:
        if isinstance(child, Directory):
            if child.size <= threshold:
                count += child.size
            count += count_threshold_directory_sizes(
                items=child.children, threshold=threshold
            )
    return count


def parse_command_output(data: str) -> Directory:
    current_directory = Directory(name="/", parent=None, children=[])
    root_directory = current_directory
    for line in data.splitlines()[1:]:
        if line == "$ cd ..":
            current_directory = current_directory.parent
        elif line.startswith("$ cd"):
            # !!! ASSUMING DIRECTORIES ARE VISITED ONLY ONCE
            # ELSE THIS SHOULD CHECK IF CHILD EXISTS IN CURRENT DIR
            name = line.split("cd ")[1]
            new_directory = Directory(name=name, parent=current_directory, children=[])
            current_directory.children.append(new_directory)
            current_directory = new_directory
        elif line == "$ ls":
            continue  # handled by this iteration
        elif line.startswith("dir"):
            continue  # don't care, cd into it first
        else:  # file info
            file_size, name = line.split()
            current_directory.children.append(File(name=name, size=int(file_size)))

    return root_directory
