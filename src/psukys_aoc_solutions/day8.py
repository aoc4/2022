class Day8:
    def __init__(self, data: str):
        self.trees = [[int(tree) for tree in line] for line in data.splitlines()]

    @property
    def height(self) -> int:
        return len(self.trees)

    @property
    def width(self) -> int:
        return len(self.trees[0])

    def solution1(self):
        visible_trees = self.height * 2 + (self.width - 2) * 2
        for y in range(1, self.height - 1):
            for x in range(1, self.width - 1):
                if self.is_visible(x, y):
                    visible_trees += 1

        return visible_trees

    def is_visible(self, x: int, y: int) -> bool:
        return (
            self.is_visible_down(x, y)
            or self.is_visible_up(x, y)
            or self.is_visible_left(x, y)
            or self.is_visible_right(x, y)
        )

    def is_visible_up(self, x: int, y: int) -> bool:
        height = self.trees[y][x]
        for check_y in range(y):
            if self.trees[check_y][x] >= height:
                return False
        return True

    def is_visible_down(self, x: int, y: int) -> bool:
        height = self.trees[y][x]
        for check_y in range(y + 1, self.height):
            if self.trees[check_y][x] >= height:
                return False
        return True

    def is_visible_left(self, x: int, y: int) -> bool:
        height = self.trees[y][x]
        for check_x in range(x):
            if self.trees[y][check_x] >= height:
                return False
        return True

    def is_visible_right(self, x: int, y: int) -> bool:
        height = self.trees[y][x]
        for check_x in range(x + 1, self.width):
            if self.trees[y][check_x] >= height:
                return False
        return True

    def solution2(self):
        max_visible_trees = 0
        for y in range(1, self.height - 1):
            for x in range(1, self.width - 1):
                max_visible_trees = max(max_visible_trees, self.get_visibility(x, y))

        return max_visible_trees

    def get_visibility(self, x: int, y: int) -> int:
        visible_trees = 1
        height = self.trees[y][x]

        # left
        if x > 0:
            visibility = 0
            for check_x in range(x - 1, -1, -1):
                visibility += 1
                if self.trees[y][check_x] >= height:
                    break
            visible_trees *= max(1, visibility)

        # right
        if x < self.width - 1:
            visibility = 0
            for check_x in range(x + 1, self.width):
                visibility += 1
                if self.trees[y][check_x] >= height:
                    break
            visible_trees *= max(1, visibility)

        # up
        if y > 0:
            visibility = 0
            for check_y in range(y - 1, -1, -1):
                visibility += 1
                if self.trees[check_y][x] >= height:
                    break
            visible_trees *= max(1, visibility)

        # down
        if y < self.height - 1:
            visibility = 0
            for check_y in range(y + 1, self.height):
                visibility += 1
                if self.trees[check_y][x] >= height:
                    break
            visible_trees *= max(1, visibility)

        return visible_trees
