from enum import Enum
from dataclasses import dataclass


class Direction(Enum):
    UP = "U"
    DOWN = "D"
    LEFT = "L"
    RIGHT = "R"


@dataclass(frozen=True)
class Instruction:
    direction: Direction
    steps: int


@dataclass(frozen=True)
class Position:
    x: int
    y: int


class Day9:
    def __init__(self, data: str):
        self.instructions: list[Instruction] = []
        for line in data.splitlines():
            direction, steps = line.split()
            self.instructions.append(
                Instruction(Direction(direction), steps=int(steps))
            )

    def solution1(self):
        head_position = Position(0, 0)
        tail_position = Position(0, 0)
        tail_visits: set(Position) = {Position(0, 0)}
        for instruction in self.instructions:
            for _ in range(instruction.steps):
                head_position = self.move_head(head_position, instruction.direction)
                tail_position = self.relative_move(head_position, tail_position)
                tail_visits.add(tail_position)
        return len(tail_visits)

    def move_head(self, position: Position, direction: Direction) -> Position:
        x, y = position.x, position.y
        if direction == Direction.UP:
            y += 1
        elif direction == Direction.DOWN:
            y -= 1
        elif direction == Direction.LEFT:
            x -= 1
        elif direction == Direction.RIGHT:
            x += 1
        else:
            raise RuntimeError("Unknown direction")

        return Position(x, y)

    def relative_move(
        self, head_position: Position, tail_position: Position
    ) -> Position:
        diff_x = head_position.x - tail_position.x
        diff_y = head_position.y - tail_position.y
        x = tail_position.x
        y = tail_position.y

        if abs(diff_x) <= 1 and abs(diff_y) <= 1:
            return tail_position  # no change as head is nearby

        if abs(diff_x) > 1:
            x += 1 * (diff_x / abs(diff_x))
            if abs(diff_y) == 1:
                y += 1 * (diff_y / abs(diff_y))
        if abs(diff_y) > 1:
            y += 1 * (diff_y / abs(diff_y))
            if abs(diff_x) == 1:
                x += 1 * (diff_x / abs(diff_x))

        return Position(x, y)

    def solution2(self):
        head_position = Position(0, 0)
        other_knots = [Position(0, 0) for _ in range(9)]
        tail_visits: set(Position) = {Position(0, 0)}
        for instruction in self.instructions:
            for _ in range(instruction.steps):
                head_position = self.move_head(head_position, instruction.direction)
                for index, knot in enumerate(other_knots):
                    if index == 0:
                        other_knots[index] = self.relative_move(head_position, knot)
                    else:
                        other_knots[index] = self.relative_move(
                            other_knots[index - 1], knot
                        )
                tail_visits.add(other_knots[-1])
        return len(tail_visits)
