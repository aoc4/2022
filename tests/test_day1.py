import pytest

from psukys_aoc_solutions.day1 import Day1


DATA = """1000
2000
3000

4000

5000
6000

7000
8000
9000

10000"""


def test_1():
    expected = 24000
    sut = Day1(data=DATA)
    assert expected == sut.solution1()


def test_2():
    expected = 45000
    sut = Day1(data=DATA)
    assert expected == sut.solution2()
