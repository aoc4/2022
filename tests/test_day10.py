import pytest

from psukys_aoc_solutions.day10 import Day10


SMALL_EXAMPLE = """noop
addx 3
addx -5"""

BIG_EXAMPLE = data = """addx 15
addx -11
addx 6
addx -3
addx 5
addx -1
addx -8
addx 13
addx 4
noop
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx 5
addx -1
addx -35
addx 1
addx 24
addx -19
addx 1
addx 16
addx -11
noop
noop
addx 21
addx -15
noop
noop
addx -3
addx 9
addx 1
addx -3
addx 8
addx 1
addx 5
noop
noop
noop
noop
noop
addx -36
noop
addx 1
addx 7
noop
noop
noop
addx 2
addx 6
noop
noop
noop
noop
noop
addx 1
noop
noop
addx 7
addx 1
noop
addx -13
addx 13
addx 7
noop
addx 1
addx -33
noop
noop
noop
addx 2
noop
noop
noop
addx 8
noop
addx -1
addx 2
addx 1
noop
addx 17
addx -9
addx 1
addx 1
addx -3
addx 11
noop
noop
addx 1
noop
addx 1
noop
noop
addx -13
addx -19
addx 1
addx 3
addx 26
addx -30
addx 12
addx -1
addx 3
addx 1
noop
noop
noop
addx -9
addx 18
addx 1
addx 2
noop
noop
addx 9
noop
noop
noop
addx -1
addx 2
addx -37
addx 1
addx 3
noop
addx 15
addx -21
addx 22
addx -6
addx 1
noop
addx 2
addx 1
noop
addx -10
noop
noop
addx 20
addx 1
addx 2
addx 2
addx -6
addx -11
noop
noop
noop"""


def test_1_small_example_cycle_1():
    expected = 1
    sut = Day10(data=SMALL_EXAMPLE)
    assert expected == sut.cycle(1)


def test_1_small_example_cycle_2():
    expected = 1
    sut = Day10(data=SMALL_EXAMPLE)
    assert expected == sut.cycle(2)


def test_1_small_example_cycle_3():
    expected = 1
    sut = Day10(data=SMALL_EXAMPLE)
    assert expected == sut.cycle(3)


def test_1_small_example_cycle_4():
    expected = 4
    sut = Day10(data=SMALL_EXAMPLE)
    assert expected == sut.cycle(4)


def test_1_small_example_cycle_5():
    expected = 4
    sut = Day10(data=SMALL_EXAMPLE)
    assert expected == sut.cycle(5)


def test_1_big_example():
    expected = 13140
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.solution1()


def test_1_big_example_cycle_20():
    expected = 21
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.cycle(20)


def test_1_big_example_cycle_60():
    expected = 19
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.cycle(60)


def test_1_big_example_cycle_100():
    expected = 18
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.cycle(100)


def test_1_big_example_cycle_140():
    expected = 21
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.cycle(140)


def test_1_big_example_cycle_180():
    expected = 16
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.cycle(180)


def test_1_big_example_cycle_220():
    expected = 18
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.cycle(220)


def test_2_big_example():
    expected = """##..##..##..##..##..##..##..##..##..##..
###...###...###...###...###...###...###.
####....####....####....####....####....
#####.....#####.....#####.....#####.....
######......######......######......####
#######.......#######.......#######....."""
    sut = Day10(data=BIG_EXAMPLE)
    assert expected == sut.solution2()
