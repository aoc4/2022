import pytest

from psukys_aoc_solutions.day12 import Day12, Point, get_neighbours

EXAMPLE = """Sabqponm
abcryxxl
accszExk
acctuvwj
abdefghi"""


def test_1_example():
    expected = 31
    sut = Day12(data=EXAMPLE)
    assert expected == sut.solution1()


def test_1_get_neighbours_top_left():
    point = Point(0, 0)
    max_point = Point(4, 4)
    expected = {Point(1, 0), Point(0, 1)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_top():
    point = Point(1, 0)
    max_point = Point(4, 4)
    expected = {Point(0, 0), Point(1, 1), Point(2, 0)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_top_right():
    point = Point(4, 0)
    max_point = Point(4, 4)
    expected = {Point(3, 0), Point(4, 1)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_right():
    point = Point(4, 1)
    max_point = Point(4, 4)
    expected = {Point(4, 0), Point(3, 1), Point(4, 2)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_right_bottom():
    point = Point(4, 4)
    max_point = Point(4, 4)
    expected = {Point(4, 3), Point(3, 4)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_bottom():
    point = Point(3, 4)
    max_point = Point(4, 4)
    expected = {Point(4, 4), Point(3, 3), Point(2, 4)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_bottom_left():
    point = Point(0, 4)
    max_point = Point(4, 4)
    expected = {Point(1, 4), Point(0, 3)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_left():
    point = Point(0, 3)
    max_point = Point(4, 4)
    expected = {Point(0, 4), Point(1, 3), Point(0, 2)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_1_get_neighbours_middle():
    point = Point(1, 1)
    max_point = Point(4, 4)
    expected = {Point(0, 1), Point(1, 0), Point(1, 2), Point(2, 1)}
    result = get_neighbours(point, max_point)
    assert expected == result


def test_2_example():
    expected = 29
    sut = Day12(data=EXAMPLE)
    assert expected == sut.solution2()
