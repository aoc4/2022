import pytest

from psukys_aoc_solutions.day13 import Day13

EXAMPLE = """[1,1,3,1,1]
[1,1,5,1,1]

[[1],[2,3,4]]
[[1],4]

[9]
[[8,7,6]]

[[4,4],4,4]
[[4,4],4,4,4]

[7,7,7,7]
[7,7,7]

[]
[3]

[[[]]]
[[]]

[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]"""


def test_1_example():
    expected = 13
    sut = Day13(data=EXAMPLE)
    assert expected == sut.solution1()


def test_1_first():
    data = """[1,1,3,1,1]
[1,1,5,1,1]"""
    expected = 1
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_second():
    data = """[[1],[2,3,4]]
[[1],4]"""
    expected = 1
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_third():
    data = """[9]
[[8,7,6]]"""
    expected = 0
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_fourth():
    data = """[[4,4],4,4]
[[4,4],4,4,4]"""
    expected = 1
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_fifth():
    data = """[7,7,7,7]
[7,7,7]"""
    expected = 0
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_sixth():
    data = """[]
[3]"""
    expected = 1
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_seventh():
    data = """[[[]]]
[[]]"""
    expected = 0
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_eighth():
    data = """[1,[2,[3,[4,[5,6,7]]]],8,9]
[1,[2,[3,[4,[5,6,0]]]],8,9]"""
    expected = 0
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_real_1():
    data = """[[[5,1,[],[8,1,3],6],[[7]]],[10,[]],[6],[[],[[0,6,4,10,5],[2,2,9],[4,4],[2,10,4,10,8]],7,7],[[5],5,[[6],[8,2],[5],[]],[9,3,2,[9,4,8,6,8]]]]
[[5]]"""
    expected = 0
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_1_real_2():
    data = """[[[5,3,[],[5,3,10],2],[[5,0,9,1]]],[[[9,0],[9,6,3]]],[[[1],1,[7,6],10],4],[2]]
[[],[[2,[9,2,6,9],10],[[2,8,10],[4,6],[1,6,4,6],[2,10,1,0,6]],6,[7,6,3,[10,6,4,5],4]],[],[2,2,[]],[2,9,[[3,3,9,6],[6,8,7,9,6],10,2,[6,9,8,7,0]],[7]]]"""
    expected = 0
    sut = Day13(data=data)
    assert expected == sut.solution1()


def test_2():
    expected = 140
    sut = Day13(data=EXAMPLE)
    assert expected == sut.solution2()
