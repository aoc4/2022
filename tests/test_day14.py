import pytest

from psukys_aoc_solutions.day14 import Day14, Point, get_points

EXAMPLE = """498,4 -> 498,6 -> 496,6
503,4 -> 502,4 -> 502,9 -> 494,9"""


def test_1_example():
    expected = 24
    sut = Day14(data=EXAMPLE)
    assert expected == sut.solution1()


def test_1_get_points_right():
    data = (Point(1, 1), Point(3, 1))
    expected = {Point(1, 1), Point(2, 1), Point(3, 1)}
    assert expected == get_points(*data)


def test_1_get_points_left():
    data = (Point(3, 1), Point(1, 1))
    expected = {Point(1, 1), Point(2, 1), Point(3, 1)}
    assert expected == get_points(*data)


def test_1_get_points_up():
    data = (Point(1, 3), Point(1, 1))
    expected = {Point(1, 1), Point(1, 2), Point(1, 3)}
    assert expected == get_points(*data)


def test_1_get_points_down():
    data = (Point(1, 1), Point(1, 3))
    expected = {Point(1, 1), Point(1, 2), Point(1, 3)}
    assert expected == get_points(*data)


def test_2():
    expected = 93
    sut = Day14(data=EXAMPLE)
    assert expected == sut.solution2()
