from psukys_aoc_solutions.day2 import Day2


def test_1_example():
    data = """A Y
B X
C Z"""
    expected = 15
    sut = Day2(data=data)
    assert expected == sut.solution1()


def test_1_paper_win():
    data = "A Y"
    expected = 8
    sut = Day2(data=data)
    assert expected == sut.solution1()


def test_1_rock_lose():
    data = "B X"
    expected = 1
    sut = Day2(data=data)
    assert expected == sut.solution1()


def test_1_scissor_draw():
    data = "C Z"
    expected = 6
    sut = Day2(data=data)
    assert expected == sut.solution1()


def test_2_example():
    data = """A Y
B X
C Z"""
    expected = 12
    sut = Day2(data=data)
    assert expected == sut.solution2()


def test_2_rock_draw():
    data = "A Y"
    expected = 4
    sut = Day2(data=data)
    assert expected == sut.solution2()


def test_2_paper_lose():
    data = "B X"
    expected = 1
    sut = Day2(data=data)
    assert expected == sut.solution2()


def test_2_scissors_win():
    data = "C Z"
    expected = 7
    sut = Day2(data=data)
    assert expected == sut.solution2()
