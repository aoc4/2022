import pytest

from psukys_aoc_solutions.day3 import Day3


def test_1_p():
    data = "vJrwpWtwJgWrhcsFMMfFFhFp"
    expected = 16
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_1_L():
    data = "jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL"
    expected = 38
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_1_P():
    data = "PmmdzqPrVvPwwTWBwg"
    expected = 42
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_1_v():
    data = "wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn"
    expected = 22
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_1_t():
    data = "ttgJtRGJQctTZtZT"
    expected = 20
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_1_s():
    data = "CrZsJsPPZsGzwwsLwLmpwMDw"
    expected = 19
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_1_two():
    data = "acAaAd"
    expected = 28
    sut = Day3(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """vJrwpWtwJgWrhcsFMMfFFhFp
jqHRNqRjqzjGDLGLrsFMfFZSrLrFZsSL
PmmdzqPrVvPwwTWBwg
wMqvLMZHhHMvwLHjbvcjnnSBnvTQFn
ttgJtRGJQctTZtZT
CrZsJsPPZsGzwwsLwLmpwMDw"""
    expected = 70
    sut = Day3(data=data)
    assert expected == sut.solution2()
