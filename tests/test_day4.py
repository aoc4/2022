import pytest

from psukys_aoc_solutions.day4 import Day4


def test_1_example():
    data = """2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8"""
    expected = 2
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_left_not_connected():
    data = "2-4,6-8"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_left_edge():
    data = "2-4,4-8"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_left_overlap():
    data = "2-4,3-8"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_left_edge_cover():
    data = "2-4,2-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_left_broad_cover():
    data = "2-4,1-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_right_not_connected():
    data = "6-8,2-4"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_right_edge():
    data = "4-8,2-4"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_right_overlap():
    data = "6-8,2-7"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_right_edge_cover():
    data = "6-8,2-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_right_broad_cover():
    data = "6-8,2-10"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_1_singles():
    data = "1-1,1-1"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution1()


def test_2_example():
    data = """2-4,6-8
2-3,4-5
5-7,7-9
2-8,3-7
6-6,4-6
2-6,4-8"""
    expected = 4
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_left_not_connected():
    data = "2-4,6-8"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_left_edge():
    data = "2-4,4-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_left_overlap():
    data = "2-4,3-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_left_edge_cover():
    data = "2-4,2-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_left_broad_cover():
    data = "2-4,1-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_right_not_connected():
    data = "6-8,2-4"
    expected = 0
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_right_edge():
    data = "4-8,2-4"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_right_overlap():
    data = "6-8,2-7"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_right_edge_cover():
    data = "6-8,2-8"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_right_broad_cover():
    data = "6-8,2-10"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()


def test_2_singles():
    data = "1-1,1-1"
    expected = 1
    sut = Day4(data=data)
    assert expected == sut.solution2()
