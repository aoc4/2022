import pytest

from psukys_aoc_solutions.day5 import (
    Day5,
    Instruction,
    parse_crates,
    parse_instructions,
)


def test_crate_parser():
    data = """    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3"""
    expected = [["Z", "N"], ["M", "C", "D"], ["P"]]
    assert expected == parse_crates(data)


def test_instruction_parser():
    data = "move 1 from 2 to 1"
    expected = [Instruction(source=2, target=1, amount=1)]
    assert expected == parse_instructions(data)


def test_1_example():
    data = """    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2"""
    expected = "CMZ"
    sut = Day5(data=data)
    assert expected == sut.solution1()


def test_1_step_1():
    data = """    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
"""
    expected = "DCP"
    sut = Day5(data=data)
    assert expected == sut.solution1()


# steps 2 and 3 have empty lines


def test_1_step_4():
    data = """        [Z]
        [N]
[M]     [D]
[C]     [P]
 1   2   3

move 1 from 1 to 2
"""
    expected = "CMZ"
    sut = Day5(data=data)
    assert expected == sut.solution1()


def test_2_example():
    data = """    [D]    
[N] [C]    
[Z] [M] [P]
 1   2   3 

move 1 from 2 to 1
move 3 from 1 to 3
move 2 from 2 to 1
move 1 from 1 to 2"""
    expected = "MCD"
    sut = Day5(data=data)
    assert expected == sut.solution2()
