import pytest

from psukys_aoc_solutions.day6 import Day6


def test_1_example_explained():
    data = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
    expected = 7
    sut = Day6(data=data)
    assert expected == sut.solution1()


def test_1_example1():
    data = "bvwbjplbgvbhsrlpgdmjqwftvncz"
    expected = 5
    sut = Day6(data=data)
    assert expected == sut.solution1()


def test_1_example2():
    data = "nppdvjthqldpwncqszvftbrmjlhg"
    expected = 6
    sut = Day6(data=data)
    assert expected == sut.solution1()


def test_1_example3():
    data = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
    expected = 10
    sut = Day6(data=data)
    assert expected == sut.solution1()


def test_1_example3():
    data = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"
    expected = 11
    sut = Day6(data=data)
    assert expected == sut.solution1()


def test_2_example1():
    data = "mjqjpqmgbljsphdztnvjfqwrcgsmlb"
    expected = 19
    sut = Day6(data=data)
    assert expected == sut.solution2()


def test_2_example2():
    data = "bvwbjplbgvbhsrlpgdmjqwftvncz"
    expected = 23
    sut = Day6(data=data)
    assert expected == sut.solution2()


def test_2_example3():
    data = "nppdvjthqldpwncqszvftbrmjlhg"
    expected = 23
    sut = Day6(data=data)
    assert expected == sut.solution2()


def test_2_example4():
    data = "nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg"
    expected = 29
    sut = Day6(data=data)
    assert expected == sut.solution2()


def test_2_example5():
    data = "zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw"
    expected = 26
    sut = Day6(data=data)
    assert expected == sut.solution2()
