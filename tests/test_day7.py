import pytest

from psukys_aoc_solutions.day7 import Day7


def test_1_example():
    data = """$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k"""
    expected = 95437
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_single_dir_under_100k():
    data = """$ cd /
$ ls
29116 f
2557 g"""
    expected = 31673
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_single_dir_over_100k():
    data = """$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d"""
    expected = 0
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_dir_walk_down_match_lower():
    data = """$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
85041 c.dat"""
    expected = 85041
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_dir_walk_down_match_upper():
    data = """$ cd /
$ ls
dir a
14514 b.txt
8556 c.dat
dir d
$ cd a
$ ls
850141 c.dat"""
    expected = 0
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_dir_walk_up_match_last():
    data = """$ cd /
$ ls
dir a
14514 b.txt
85562 c.dat
$ cd a
$ ls
850411 c.dat
dir b
dir c
$ cd b
$ ls
123451 aha.txt
$ cd ..
$ cd c
$ ls
54321 next.po"""
    expected = 54321
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_dir_walk_up_match_last_and_root():
    data = """$ cd /
$ ls
dir a
14514 b.txt
8556 c.dat
$ cd a
$ ls
850411 c.dat
dir b
dir c
$ cd b
$ ls
123451 aha.txt
$ cd ..
$ cd c
$ ls
54321 next.po"""
    expected = 54321
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_dir_walk_up_match_last_siblings():
    data = """$ cd /
$ ls
dir a
14514 b.txt
85562 c.dat
$ cd a
$ ls
850411 c.dat
dir b
dir c
$ cd b
$ ls
12345 aha.txt
$ cd ..
$ cd c
$ ls
54321 next.po"""
    expected = 66666
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_1_dir_walk_up_match_subroot_only():
    data = """$ cd /
$ ls
dir a
14514 b.txt
85562 c.dat
$ cd a
$ ls
85041 c.dat
dir b
dir c
$ cd b
$ ls
123451 aha.txt
$ cd ..
$ cd c
$ ls
543212 next.po"""
    expected = 0
    sut = Day7(data=data)
    assert expected == sut.solution1()


def test_2():
    data = """$ cd /
$ ls
dir a
14848514 b.txt
8504156 c.dat
dir d
$ cd a
$ ls
dir e
29116 f
2557 g
62596 h.lst
$ cd e
$ ls
584 i
$ cd ..
$ cd ..
$ cd d
$ ls
4060174 j
8033020 d.log
5626152 d.ext
7214296 k"""
    expected = 24933642
    sut = Day7(data=data)
    assert expected == sut.solution2()
