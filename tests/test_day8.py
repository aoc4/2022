import pytest

from psukys_aoc_solutions.day8 import Day8


def test_1_example():
    data = """30373
25512
65332
33549
35390"""
    expected = 21
    sut = Day8(data=data)
    assert expected == sut.solution1()


def test_1_minimal():
    data = """123
456
789"""
    expected = 9  # top and left and edges
    sut = Day8(data=data)
    assert expected == sut.solution1()


def test_1_all():
    data = """11111
12221
12321
12221
11111"""
    expected = 25
    sut = Day8(data=data)
    assert expected == sut.solution1()


def test_1_only_edge():
    data = """33333
32223
32323
32223
33333"""
    expected = 16
    sut = Day8(data=data)
    assert expected == sut.solution1()


def test_1_all_same_edge():
    data = """33333
33333
33333
33333
33333"""
    expected = 16
    sut = Day8(data=data)
    assert expected == sut.solution1()


def test_2_example():
    data = """30373
25512
65332
33549
35390"""
    expected = 2 * 2 * 1 * 2
    sut = Day8(data=data)
    assert expected == sut.solution2()


def test_2_minimal():
    data = """123
456
789"""
    expected = 1  # top and left and edges
    sut = Day8(data=data)
    assert expected == sut.solution2()


def test_2_all():
    data = """11111
12221
12321
12221
11111"""
    expected = 2 * 2 * 2 * 2
    sut = Day8(data=data)
    assert expected == sut.solution2()


def test_1_only_edge():
    data = """33333
32223
32323
32223
33333"""
    expected = 2 * 2 * 2 * 2
    sut = Day8(data=data)
    assert expected == sut.solution2()


def test_1_all_same_edge():
    data = """33333
33333
33333
33333
33333"""
    expected = 1
    sut = Day8(data=data)
    assert expected == sut.solution2()
