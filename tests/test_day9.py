import pytest

from psukys_aoc_solutions.day9 import Day9


def test_1_example():
    data = """R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2"""
    expected = 13
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_right():
    data = "R 4"
    expected = 4
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_up():
    data = "U 3"
    expected = 3
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_left():
    data = "L 5"
    expected = 5
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_down():
    data = "D 6"
    expected = 6
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_square_no_move():
    data = """R 1
U 1
L 1
D 1"""
    expected = 1
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_square_2():
    data = """R 2
U 2
L 2
D 2"""
    expected = 5
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_square_3():
    data = """R 3
U 3
L 3
D 3"""
    expected = 9
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_1_move_square_4():
    data = """R 4
U 4
L 4
D 4"""
    expected = 13
    sut = Day9(data=data)
    assert expected == sut.solution1()


def test_2_small_example():
    data = """R 4
U 4
L 3
D 1
R 4
D 1
L 5
R 2"""
    expected = 1
    sut = Day9(data=data)
    assert expected == sut.solution2()


def test_2_larger_example():
    data = """R 5
U 8
L 8
D 3
R 17
D 10
L 25
U 20"""
    expected = 36
    sut = Day9(data=data)
    assert expected == sut.solution2()
